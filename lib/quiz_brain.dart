import 'question.dart';

class QuizBrain {
  int _questionNumber = 0;

  List<Question> _questionBank = [
    Question('Millennium Falcon is han solo\'s ship', true, 'https://cdn.europosters.eu/image/750/posters/star-wars-millennium-falcon-i11810.jpg'),
    Question('Chopsticks is Jedi\'s main weapon.', false, 'https://www.dhresource.com/0x0/f2/albu/g6/M01/17/55/rBVaSFvzoUqAAX8UAADj0FQqiiM277.jpg'),
    Question('Bobafett become darth vader.', false, 'https://nerdist.com/wp-content/uploads/2019/05/Darth-Vader-1200x676.jpg'),
    Question('Anakin Skywalker became Darth Sidious', false, 'https://i.pinimg.com/originals/ce/d8/44/ced8441043bc2a66a3da0137bcdb49d9.jpg'),
    Question('Yoda carries a green light saber.', true, 'http://www.jedinews.co.uk/wp-content/uploads/2017/06/yoda-lightsaber-rots-696x467.jpg'),
    Question('May the force be with you is a tag line from the Jedi', true, 'https://i.ytimg.com/vi/pHyk54dBd_Y/maxresdefault.jpg'),
    Question(
        'X Wing has 6 engines.',
        false, 'https://cdna.artstation.com/p/assets/images/images/011/440/622/large/stephan-deutsch-xwing-space-blueplanet-open-filmic.jpg'),
    Question(
        'Count Doku was Yoda\'s apprentice',
        true, 'https://carboncostume.com/wordpress/wp-content/uploads/2018/10/count-dooku.jpg'),
    Question(
        'Padme Amiadula is Luke and Leia\'s mother',
        false, 'https://i.ytimg.com/vi/4okzpXC6Fl4/maxresdefault.jpg'),
    Question(
        'Total 12 members made out of Jedi council.',
        true, 'https://vignette.wikia.nocookie.net/starwars/images/a/ac/High_Council_Chamber.png'),
    Question('Sith is the dark side.', true, 'https://usercontent1.hubstatic.com/6478280.jpg'),
    Question(
        'Darth vader want to freeze Luke Skywalker as a capture plan',
        true, 'https://i.pinimg.com/736x/7a/f8/8a/7af88a0bf2696b9abe8e87cb16dcb8a0.jpg'),
    Question(
        'Boba tea capture han solo',
        false, 'https://cdn11.bigcommerce.com/s-4c9vl923de/images/stencil/1024x1024/products/115/378/original__27419.1483977430.jpg'),
    Question(
        ' C-3P0 fluent more than 6 million languages',
        true, 'https://www.thewrap.com/wp-content/uploads/2019/10/C-3PO-Star-Wars-The-Rise-of-Skywalker-Final-Trailer.jpg'),
  ];

  void nextQuestion(){
    if(_questionNumber < _questionBank.length - 1){
      _questionNumber++;
    }
  }

  String getQuestionText(){
    return _questionBank[_questionNumber].questionText;
  }

  String getQuestionImageUrl(String text){
    print('Text > ' + text);
    var foundObj = _questionBank.singleWhere((p)=>p.questionText == text
        , orElse: () => null);
    print(foundObj.questionText);
    return foundObj.imageUrl;
  }

  bool getCorrectAnswer(){
    return _questionBank[_questionNumber].questionAnswer;
  }

  int getTotalQuestions(){
    return _questionBank.length;
  }

  bool isFinished(){
    if(_questionNumber >= _questionBank.length -1){
      return true;
    } else {
      return false;
    }
  }

  void reset(){
    _questionNumber=0;
  }
}