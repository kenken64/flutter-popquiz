import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'quiz_brain.dart';

QuizBrain quizBrain = QuizBrain();

class QuizPage extends StatefulWidget {
  @override
  _QuizPageState createState() => _QuizPageState();
}

class _QuizPageState extends State<QuizPage>{
  List<Icon> scoreKeeper = [];
  int correctCounter = 0;

  void checkAnswer(bool userPickedAnswer){
    bool correctAnswer = quizBrain.getCorrectAnswer();

    setState(() {
      var congratzMesg = 'Well done!';
      if (quizBrain.isFinished() == true) {
        if(correctCounter == 0 && scoreKeeper.length == scoreKeeper.length){
          congratzMesg = "Yikes!";
        }
        Alert(
          context: context,
          type: AlertType.success,
          title: "Congratulations!",
          desc: "You have reached the end of the quiz. Your score is ${correctCounter}/${scoreKeeper.length}.${congratzMesg}",
          buttons: [
            DialogButton(
              child: Text(
                "Restart Quiz",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () => Navigator.pop(context),
              width: 120,
            )
          ],
        ).show();

        quizBrain.reset();
        scoreKeeper = [];
        correctCounter= 0;

      } else {
        if (userPickedAnswer == correctAnswer) {
          scoreKeeper.add(Icon(
              Icons.check,
              color: Colors.green));
          correctCounter++;
        } else {
          scoreKeeper.add(Icon(
              Icons.close,
              color: Colors.red));
        }
      }

      quizBrain.nextQuestion();

    });
  }

  @override
  Widget build(BuildContext context){
    quizBrain.getQuestionImageUrl(quizBrain.getQuestionText());
    return 
      Container(
        decoration: BoxDecoration(
            image: DecorationImage(image: NetworkImage(quizBrain.getQuestionImageUrl(quizBrain.getQuestionText()))
                , fit: BoxFit.cover),
          ),
        child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            flex:5,
            child: Padding(
                padding: EdgeInsets.all(10.0),
                child: Container(
                  child: Text(
                      quizBrain.getQuestionText(),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 25.0,
                        color: Colors.white,
                      )
                  ), alignment:FractionalOffset(0.5, 0.2),
                ),
            ), 
          ),
          Expanded(
            child:Container(
              
          ),),
          Expanded(
              child: Padding(
                  padding: EdgeInsets.all(15.0),
                  child: FlatButton(
                      color: Colors.green,
                      child: Text(
                          'True',
                          style: TextStyle(
                            fontSize: 20.0,
                            color: Colors.white,
                          )
                      ),
                      onPressed: () {
                        // user picked false.
                        checkAnswer(true);
                      }
                  )
              )
          ),
          Expanded(
              child: Padding(
                  padding: EdgeInsets.all(15.0),
                  child: FlatButton(
                      color: Colors.red,
                      child: Text(
                          'False',
                          style: TextStyle(
                            fontSize: 20.0,
                            color: Colors.white,
                          )
                      ),
                      onPressed: () {
                        // user picked false.
                        checkAnswer(false);
                      }
                  )
              )
          ),
          Row(
            children: scoreKeeper,
          )

        ]
      ),
    );
      
  }
}