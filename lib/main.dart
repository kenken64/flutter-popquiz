// Copyright 2019 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vector_math/vector_math_64.dart';
import './home.dart';

void main() => runApp(MyApp());

final yellow = Color(0xFFFFFF00);

final textStyle = TextStyle(
  color: yellow,
  fontSize: 24,
);

final textStyleTitle = TextStyle(
  color: yellow,
  fontSize: 28,
);

final crawlString1 = """
Star Wars Pop Quiz
""";
final crawlString2 = """
\nAre you just now getting into Star Wars? Better late than never! There's a lot to know about this amazing franchise. 
Here's an easy Star Wars trivia quiz for you to try - see how much you know right now!.
\nStar Wars is an American epic space-opera media franchise created by George Lucas. 
\nThe franchise began with the eponymous 1977 
film and quickly became a worldwide pop-culture phenomenon.
""";

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Color(0xFF000000),
        body:
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(image: AssetImage("images/falcon.jpg"), fit: BoxFit.cover),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: CrawlPage(),
          ),
        ),
      ),
      routes: <String, WidgetBuilder>{
        '/HomeScreen': (BuildContext context) => new QuizPage()
      },
    );
  }
}

class CrawlPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Flexible(
        flex: 5,
        child: Perspective(child: Crawler()),
      ),
      Flexible(
        flex: 1,
        child: Column(),
      ),
    ]);
  }
}

class Crawler extends StatefulWidget {
  final crawlDuration = const Duration(seconds: 20);

  @override
  createState() => _CrawlerState();
}

class _CrawlerState extends State<Crawler> {
  final _scrollController = ScrollController();

  _scrollListener() {
    if (_scrollController.offset >= _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      setState(() {
        print("reach the bottom");
        Navigator.of(context).pushReplacementNamed('/HomeScreen');
      });
    }
  }
  void gotoNextPage(){
    _scrollController.animateTo(
        _scrollController.position.maxScrollExtent,
        duration: widget.crawlDuration,
        curve: Curves.linear);
    _scrollController.addListener(_scrollListener);
  }

  @override
  void initState() {
    Future.delayed(
        const Duration(milliseconds: 2000),gotoNextPage);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return SuperSize(
      height: 1279,
      child: ListView(
        controller: _scrollController,
        children: [
          SizedBox(height: height),
          Text(
            crawlString1,
            style: textStyleTitle,
            textAlign: TextAlign.center,
          ),
          Text(
            crawlString2,
            style: textStyle,
            textAlign: TextAlign.center,
          ),
          SizedBox(height: height),
        ],
      ),
    );
  }
}

class SuperSize extends StatelessWidget {
  SuperSize({this.child, this.height = 1000});
  final Widget child;
  final double height;

  @override
  Widget build(BuildContext context) {
    return OverflowBox(
      minHeight: height,
      maxHeight: height,
      child: child,
    );
  }
}

class Perspective extends StatelessWidget {
  Perspective({this.child});
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Transform(
      transform: Matrix4.identity()
        ..setEntry(3, 2, 0.002)
        ..rotateX(-3.14 / 3.5),
      alignment: FractionalOffset.center,
      child: child,
    );
  }
}